// App.js

import React, { useState, useEffect } from "react";
import ProductList from "./components/ProductList/ProductList";
import Header from "./components/Header/Header";
import "./App.scss";
import { Routes, Route } from "react-router-dom";
import { Cart } from "./components/pages/Cart";
import { Favorites } from "./components/pages/Favorites";
import { useDispatch, useSelector } from "react-redux";
import { fetchData } from "./components/redux/productActions";
import { ProductViewProvider } from "./components/ProductList/ProductViewContext";

function App() {
  //const [products, setProducts] = useState([]);
  const [cartItems, setCartItems] = useState([]);
  const [favoriteItems, setFavoriteItems] = useState([]);
  const [cartCount, setCartCount] = useState(0);
  const [favoriteCount, setFavoriteCount] = useState(0);

  const dispatch = useDispatch();
  const products = useSelector((state) => state.product.products);

  useEffect(() => {
    dispatch(fetchData());
  }, [dispatch]);

  // useEffect(() => {
  //   const fetchData = async () => {
  //     try {
  //       const response = await axios.get("/products.json");
  //       setProducts(response.data);
  //     } catch (error) {
  //       console.error("Error fetching product data:", error);
  //     }
  //   };

  //   fetchData();
  // }, []);

  useEffect(() => {
    const storedCartItems = JSON.parse(localStorage.getItem("cartItems")) || [];
    const storedFavoriteItems =
      JSON.parse(localStorage.getItem("favoriteItems")) || [];

    setCartItems(storedCartItems);
    setFavoriteItems(storedFavoriteItems);
    setCartCount(storedCartItems.length);
    setFavoriteCount(storedFavoriteItems.length);
  }, []);

  const updateCartCount = (count) => {
    setCartCount(count);
    setCartItems(JSON.parse(localStorage.getItem("cartItems")) || []);
  };

  const updateFavoriteCount = (count) => {
    setFavoriteCount(count);
    setFavoriteItems(JSON.parse(localStorage.getItem("favoriteItems")) || []);
  };

  //console.log(cartItems);
  //console.log(favoriteItems);

  return (
    <>
    <ProductViewProvider>
      <div>
        <Header cartItemsCount={cartCount} favoriteItemsCount={favoriteCount} />
      </div>
      <Routes>
        <Route
          path="/"
          element={
            <div>
              <ProductList
                products={products}
                updateCartCount={updateCartCount}
                updateFavoriteCount={updateFavoriteCount}
              />
            </div>
          }
        />
        <Route
          path="/cart"
          element={
            <Cart cartItems={cartItems} updateCartCount={updateCartCount} />
          }
        />
        <Route
          path="/favorites"
          element={
            <Favorites
              favoriteItems={favoriteItems}
              updateFavoriteCount={updateFavoriteCount}
            />
          }
        />
      </Routes>
      </ProductViewProvider>
    </>
  );
}

export default App;
