// ProductCard.js

import React, { useState } from "react";
import { FaStar, FaTimes } from "react-icons/fa";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import PropTypes from "prop-types";
import { useDispatch } from "react-redux";
import { openModal, closeModal } from "../redux/modalActions";
import { useProductView } from '../ProductList/ProductViewContext';

const ProductCard = ({
  product,
  updateCartCount,
  updateFavoriteCount,
  showFaTimes = false,
  showButton = true,
  buttonText,
  showFavorite = true,
  handleRemoveFromCart,
  handleRemoveFromFavorites,
}) => {
  const [isFavorite, setIsFavorite] = useState(
    JSON.parse(localStorage.getItem("favoriteItems"))?.some(
      (item) => item.id === product.id
    ) || false
  );
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isAddedToCart, setIsAddedToCart] = useState(
    JSON.parse(localStorage.getItem("cartItems"))?.some(
      (item) => item.id === product.id
    ) || false
  );

  const dispatch = useDispatch();
  //const isAnyModalOpen = useSelector((state) => state.modal.isAnyModalOpen);

  const openModalHandler = () => {
    dispatch(openModal());
  };

  const closeModalHandler = () => {
    dispatch(closeModal());
  };

  const handleOpenModal = () => {
    setIsModalOpen(true);
    openModalHandler();
  };

  const handleConfirmAddToCart = () => {
    const cartItems = JSON.parse(localStorage.getItem("cartItems")) || [];
    cartItems.push(product);
    localStorage.setItem("cartItems", JSON.stringify(cartItems));

    setIsAddedToCart(true);
    setIsModalOpen(false);
    updateCartCount(cartItems.length);
    closeModalHandler();
  };

  const handleCloseModal = () => {
    setIsModalOpen(false);
    closeModalHandler();
  };

  const handleToggleFavorite = () => {
    const favoriteItems =
      JSON.parse(localStorage.getItem("favoriteItems")) || [];
    if (!isFavorite) {
      favoriteItems.push(product);
    } else {
      const updatedFavorites = favoriteItems.filter(
        (item) => item.id !== product.id
      );
      localStorage.setItem("favoriteItems", JSON.stringify(updatedFavorites));
    }

    localStorage.setItem("favoriteItems", JSON.stringify(favoriteItems));
    setIsFavorite(!isFavorite);

    updateFavoriteCount(favoriteItems.length);
  };

  const { isTableView } = useProductView();

  return (
    <div className={`product-card ${isTableView ? 'table-view' : 'card-view'}`}>
      {showFaTimes && (
        <FaTimes className="remove-icon" onClick={handleOpenModal} />
      )}
      <img src={product.image} alt={product.name} />
      <div className={`product-info ${isTableView ? 'table-view' : 'card-view'}`}>
        <h3>Name:{product.name}</h3>
        <p>Price:${product.price}</p>
        <p>Article:{product.article}</p>
        <p>Color:{product.color}</p>
        <div className="product-actions">
          {showButton && (
            <Button
              backgroundColor={
                buttonText ? "red" : isAddedToCart ? "gray" : "green"
              }
              text={
                buttonText
                  ? buttonText
                  : isAddedToCart
                  ? "Added to Cart"
                  : "Add to Cart"
              }
              onClick={handleOpenModal}
              disabled={isAddedToCart}
            />
          )}

          {showFavorite && (
            <FaStar
              style={{ color: isFavorite ? "gold" : "gray" }}
              onClick={() => {
                handleToggleFavorite();
                if (handleRemoveFromFavorites) {
                  handleRemoveFromFavorites(product.id);
                }
              }}
            />
          )}
        </div>
      </div>

      {isModalOpen && (
        <Modal
          header="Confirmation"
          closeButton={true}
          text={
            !handleRemoveFromCart
              ? `Do you want to add ${product.name} to your cart?`
              : `Do you want to remove ${product.name} from your cart?`
          }
          actions={
            <>
              <Button
                backgroundColor="brown"
                text="Confirm"
                onClick={
                  !handleRemoveFromCart
                    ? handleConfirmAddToCart
                    : () => {
                        handleRemoveFromCart(product.id);
                        handleCloseModal();
                      }
                }
              />
              <Button
                backgroundColor="gray"
                text="Cancel"
                onClick={handleCloseModal}
              />
            </>
          }
          onClose={handleCloseModal}
        />
      )}
    </div>
  );
};

ProductCard.propTypes = {
  product: PropTypes.object.isRequired,
  updateCartCount: PropTypes.func,
  updateFavoriteCount: PropTypes.func,
  showButton: PropTypes.bool,
  buttonText: PropTypes.string,
  showFavorite: PropTypes.bool,
  handleRemoveFromCart: PropTypes.func,
  handleRemoveFromFavorites: PropTypes.func,
};

export default ProductCard;
