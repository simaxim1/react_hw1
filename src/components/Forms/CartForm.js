// CartForm.js

import React, { useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { PatternFormat } from "react-number-format";
import Alert from "@mui/material/Alert/Alert";
import CssBaseline from "@mui/material/CssBaseline";

const CartForm = ({ onCheckout }) => {
  const [showAlert, setShowAlert] = useState(false);
  const formik = useFormik({
    initialValues: {
      firstName: "",
      lastName: "",
      age: "",
      address: "",
      mobile: "",
    },
    validationSchema: Yup.object({
      firstName: Yup.string()
        .matches(/^[^0-9]*$/, "Ім'я не повинно містити цифр та символів")
        .min(2, "Дуже коротке Ім'я!")
        .max(25, "Занадто довге Ім'я!")
        .required("Додайте, будь-ласка. Ім'я"),
      lastName: Yup.string()
        .matches(/^[^0-9]*$/, "Прізвище не повинно містити цифр та символів")
        .min(3, "Дуже коротке Призвище!")
        .max(25, "Занадто довге Призвище!")
        .required("Додайте. будь ласка, Прізвище"),
      age: Yup.number()
        .min(16, "Ви маєте бути повнолітнім")
        .max(100, "Вам мають допомагати онуки")

        .required("Вкажіть, будь ласка, вік"),
      address: Yup.string().required("Вкажіть, будь ласка, адресу доставки"),
      mobile: Yup.string()
        .matches(/^\(0[0-9]{2}\)[0-9]{3}-[0-9]{2}-[0-9]{2}$/, "Невірний номер")
        .required("Вкажіть, будь ласка, телефон"),
    }),
    onSubmit: (values) => {
      onCheckout(values);
      formik.resetForm();
      setShowAlert(true);
      <Alert severity="success">This is a success alert — check it out!</Alert>;
    },
  });

  return (
    <>
      <form onSubmit={formik.handleSubmit}>
        <p>Оформлення покупки</p>
        <>
          <label htmlFor="firstName">Ім'я:</label>
          <input
            type="text"
            placeholder="Ім'я..."
            id="firstName"
            name="firstName"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.firstName}
            autoComplete="given-name"
          />
          {formik.touched.firstName && formik.errors.firstName ? (
            <div className="error">{formik.errors.firstName}</div>
          ) : null}
        </>
        <>
          <label htmlFor="lastName">Прізвище:</label>
          <input
            type="text"
            id="lastName"
            name="lastName"
            placeholder="Прізвище..."
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.lastName}
            autoComplete="family-name"
          />
          {formik.touched.lastName && formik.errors.lastName ? (
            <div className="error">{formik.errors.lastName}</div>
          ) : null}
        </>
        <>
          <label htmlFor="age">Вік:</label>
          <input
            type="text"
            id="age"
            name="age"
            placeholder="Вік..."
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.age}
            autoComplete="user-age"
          />
          {formik.touched.age && formik.errors.age ? (
            <div className="error">{formik.errors.age}</div>
          ) : null}
        </>
        <>
          <label htmlFor="address">Адреса доставки:</label>
          <input
            type="text"
            id="address"
            name="address"
            placeholder="Адреса..."
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.address}
            autoComplete="street-address"
          />
          {formik.touched.address && formik.errors.address ? (
            <div className="error">{formik.errors.address}</div>
          ) : null}
        </>
        <>
          <label htmlFor="mobile">Мобільний телефон:</label>
          <PatternFormat
            format="(0##)###-##-##"
            mask="_"
            id="mobile"
            name="mobile"
            placeholder="(0**)***-**-**"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.mobile}
            autoComplete="tel"
          />
          {formik.touched.mobile && formik.errors.mobile ? (
            <div className="error">{formik.errors.mobile}</div>
          ) : null}
        </>
        <button type="submit">Checkout</button>
      </form>
      {showAlert && (
        <>
          <CssBaseline />
          <Alert
            className="alert"
            severity="success"
            onClose={() => setShowAlert(false)}
          >
            Дякуємо за замовлення!
          </Alert>
        </>
      )}
    </>
  );
};

export default CartForm;
