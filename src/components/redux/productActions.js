import axios from 'axios';


export const fetchDataSuccess = (data) => ({
  type: 'FETCH_DATA_SUCCESS',
  payload: data,
});


export const fetchData = () => {
  return async (dispatch) => {
    try {
      const response = await axios.get('/products.json');
      dispatch(fetchDataSuccess(response.data));
    } catch (error) {
      console.error('Error fetching product data:', error);
    }
  };
};