//reducers.js

import { combineReducers } from 'redux';
import cartReducer from './cartReducer';
import favoriteReducer from './favoriteReducer';
import {productReducer} from './productReducer';
import modalReducer from './modalReducer';
import formReducer from './formReducer';


const rootReducer = combineReducers({
  cart: cartReducer,
  favorite: favoriteReducer,
  product: productReducer,
  modal: modalReducer,
  form: formReducer, 
});





export default rootReducer;
