// productReducer.js

const initialState = {
    products: [],
  };

export const productReducer = (state = initialState, action) => {
    switch (action.type) {
      case 'FETCH_DATA_SUCCESS':
        return {
          ...state,
          products: action.payload,
        };
      default:
        return state;
    }
  };