// formReducer.js

const removedItemsReducer = (state = [], action) => {
    switch (action.type) {
      case 'ADD_REMOVED_ITEM':
        return [...state, action.payload];
      case 'CLEAR_REMOVED_ITEMS':
        return state;
      default:
        return state;
    }
  };
  
  export default removedItemsReducer;