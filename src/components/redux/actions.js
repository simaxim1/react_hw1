// cart actions

export const addToCart = (productid) => {
  return {
    type: "ADD_TO_CART",
    payload: productid,
  };
};

export const removeFromCart = (productId) => {
  return {
    type: "REMOVE_FROM_CART",
    payload: productId,
  };
};

export const clearCart = () => {
  return {
    type: "CLEAR_CART",
  };
};

export const updateLocalStorage = (cartItems) => {
  return {
    type: "UPDATE_LOCAL_STORAGE",
    payload: cartItems,
  };
};

// favorite actions

export const addToFavorite = (productid) => {
  return {
    type: "ADD_TO_FAVORITE",
    payload: productid,
  };
};

export const removeFromFavorite = (productId) => {
  return {
    type: "REMOVE_FROM_FAVORITE",
    payload: productId,
  };
};

