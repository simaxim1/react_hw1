// ProductList.js

import React from 'react';
import ProductCard from '../ProductCard/ProductCard';
import { useProductView } from './ProductViewContext';

const ProductList = ({ products, updateCartCount, updateFavoriteCount }) => {
  const { isTableView } = useProductView();
  return (
    <div className={`product-list ${isTableView ? 'table-view' : 'card-view'}`}>
      {products.map((product) => (
        <ProductCard
          key={product.id}
          product={product}
          updateCartCount={updateCartCount}
          updateFavoriteCount={updateFavoriteCount}
        />
      ))}
    </div>
  );
};

export default ProductList;
