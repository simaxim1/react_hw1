// ProductViewContext

import React, { createContext, useState, useContext } from "react";

const ProductViewContext = createContext();

export const useProductView = () => {
  const context = useContext(ProductViewContext);
  if (!context) {
    throw new Error("useProductView must be used within a ProductViewProvider");
  }
  return context;
};

export const ProductViewProvider = ({ children }) => {
  const [isTableView, setIsTableView] = useState(true);

  const toggleView = () => {
    setIsTableView((prev) => !prev);
  };

  return (
    <ProductViewContext.Provider value={{ isTableView, toggleView }}>
      {children}
    </ProductViewContext.Provider>
  );
};
