// Button.js

import React from 'react';

const Button = ({ backgroundColor, text, onClick }) => {
  return (
    <button
      className="custom-button"
      style={{ backgroundColor }}
      onClick={onClick}
    >
      {text}
    </button>
  );
};

export default Button;
