// cartReducer.test.js

import cartReducer from '../redux/cartReducer';

test('ADD_TO_CART action adds item to the cart', () => {
  const initialState = {
    cartItems: [],
  };
  const action = {
    type: 'ADD_TO_CART',
    payload: { id: 1, name: 'Product 1', price: 10 },
  };
  const newState = cartReducer(initialState, action);
  expect(newState.cartItems).toHaveLength(1);
  expect(newState.cartItems[0].name).toBe('Product 1');
});

test('REMOVE_FROM_CART action removes item from the cart', () => {
  const initialState = {
    cartItems: [{ id: 1, name: 'Product 1', price: 10 }],
  };
  const action = {
    type: 'REMOVE_FROM_CART',
    payload: 1,
  };
  const newState = cartReducer(initialState, action);
  expect(newState.cartItems).toHaveLength(0);
});