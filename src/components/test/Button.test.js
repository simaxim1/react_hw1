// Button.test.js

import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Button from '../Button/Button';
import '@testing-library/jest-dom/extend-expect';


describe('Button component', () => {

  it('Button renders correctly', () => {
    const { getByText } = render(<Button text="Click me" />);
    const buttonElement = getByText(/Click me/i);
    expect(buttonElement).toBeInTheDocument();
  });
  
  it('Button click event works', () => {
    const onClickMock = jest.fn();
    const { getByText } = render(<Button text="Click me" onClick={onClickMock} />);
    const buttonElement = getByText(/Click me/i);
    fireEvent.click(buttonElement);
    expect(onClickMock).toHaveBeenCalledTimes(1);
  });
  
  it('Button snapshot', () => {
    const button = render(<Button />);
    expect(button).toMatchSnapshot();
  });
});


