// Modal.test.js

import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import Modal from '../Modal/Modal';
import '@testing-library/jest-dom/extend-expect';

describe('Modal component', () => {
  it('renders modal with header and text correctly', () => {
    render(
      <Modal
        header="Test Header"
        text="Test Text"
        actions={<button>Test Button</button>}
        onClose={() => {}}
      />
    );

    const headerElement = screen.getByText('Test Header');
    const textElement = screen.getByText('Test Text');
    const buttonElement = screen.getByText('Test Button');

    expect(headerElement).toBeInTheDocument();
    expect(textElement).toBeInTheDocument();
    expect(buttonElement).toBeInTheDocument();
  });

  it('calls onClose when overlay is clicked', () => {
    const onCloseMock = jest.fn();
    render(
      <Modal
        header="Test Header"
        text="Test Text"
        actions={<button>Test Button</button>}
        onClose={onCloseMock}
      />
    );
  
    const overlayElement = screen.getByTestId('overlay');
    fireEvent.click(overlayElement);
  
    expect(onCloseMock).toHaveBeenCalled();
  });
  
  it('calls action function when action button is clicked', () => {
    const actionMock = jest.fn();
    render(
      <Modal
        header="Test Header"
        text="Test Text"
        actions={<button onClick={actionMock}>Test Button</button>}
        onClose={() => {}}
      />
    );

    const buttonElement = screen.getByText('Test Button');
    fireEvent.click(buttonElement);

    expect(actionMock).toHaveBeenCalled();
  });

  it('Modal snapshot', () => {
    const modal = render(<Modal />);
    expect(modal).toMatchSnapshot();
  });
});