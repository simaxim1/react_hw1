// Modal.js

import React, {useRef} from 'react';
import './Modal.scss';

const Modal = ({ header, closeButton, text, actions, onClose }) => {
  const modalRef = useRef();

  const handleOverlayClick = (e) => {
    if (modalRef.current && modalRef.current.contains(e.target)) {
      return;
    }
    onClose();
  };

  return (
    <div className="modal-wrapper" data-testid="overlay" onClick={handleOverlayClick}>
      <div className="modal" ref={modalRef}>
        <div className="modal-header">
          <span>{header}</span>
          {closeButton && <span className="close-button" data-testid="close-button" onClick={onClose}>&times;</span>}
        </div>
        <div className="modal-main">
          {text}
        </div>
        <div className="modal-actions">{actions}</div>
      </div>
    </div>
  );
};

export default Modal;

