// Header.js

import React from "react";
import { FaShoppingCart, FaStar,FaThLarge,FaThList } from "react-icons/fa";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";
import { useProductView } from "../ProductList/ProductViewContext";

const Header = ({ cartItemsCount, favoriteItemsCount }) => {
  const { toggleView, isTableView } = useProductView();
  return (
    <header className="app-header">
      <h1>Web Store</h1>
      <div className="header-icons">
      <div>
          <span>
          <NavLink to="/" >Home</NavLink>
          </span>
        </div>
        <div>
          <span>
          <NavLink to="/cart" >Cart</NavLink>
            <FaShoppingCart />
          </span>{" "}
          {cartItemsCount}
        </div>
        <div>
          <span>
          <NavLink to="/favorites" >Favorites</NavLink>
            <FaStar />
          </span>{" "}
          {favoriteItemsCount}
        </div>
        <div>
          <span id="header-productview"> 
            {isTableView ? (
              <FaThLarge onClick={toggleView} />
            ) : (
              <FaThList onClick={toggleView} />
            )}
          </span>
        </div>
      </div>
    </header>
  );
};

Header.propTypes = {
  cartItemsCount: PropTypes.number.isRequired,
  favoriteItemsCount: PropTypes.number.isRequired,
};

Header.defaultProps = {
  cartItemsCount: 0,
  favoriteItemsCount: 0,
};

export default Header;
