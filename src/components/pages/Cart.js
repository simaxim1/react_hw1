// Cart.js

import React from "react";
import ProductCard from "../ProductCard/ProductCard";
import { useDispatch } from "react-redux";
import {
  clearCart,
  removeFromCart,
  updateLocalStorage,
} from "../redux/actions";
import CartForm from "../Forms/CartForm";
import { useProductView } from '../ProductList/ProductViewContext';


const Cart = ({ cartItems, updateCartCount }) => {
  const dispatch = useDispatch();

  const handleRemoveFromCart = (productId) => {
    dispatch(removeFromCart(productId));
    const updatedCartItems = cartItems.filter((item) => item.id !== productId);

    localStorage.setItem("cartItems", JSON.stringify(updatedCartItems));

    updateCartCount(updatedCartItems.length);

    dispatch(updateLocalStorage(updatedCartItems));
  };

  const handleCheckout = (formData) => {
    console.log("Замовлення:", formData);
    dispatch(clearCart());

    localStorage.setItem("cartItems", JSON.stringify([]));
    updateCartCount(0);

    console.log("Придбані товари:", cartItems);

  };

  const { isTableView } = useProductView();

  return (
    <div className="cart">
      <h2>Products in Cart</h2>
      {<CartForm onCheckout={handleCheckout} />}
      {cartItems.length === 0 ? (
        <p>Your cart is empty.</p>
      ) : (
        <div className={`product-list ${isTableView ? 'table-view' : 'card-view'}`}>
          {cartItems.map((product) => (
            <ProductCard
              key={product.id}
              product={product}
              updateCartCount={updateCartCount}
              handleRemoveFromCart={handleRemoveFromCart}
              handleCheckout={handleCheckout}
              showFaTimes={true}
              buttonText={"Remove from Cart"}
              showFavorite={false}
            />
          ))}
        </div>
      )}
    </div>
  );
};

export { Cart };
