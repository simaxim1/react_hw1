// Favorites.js

import React from "react";
import ProductCard from "../ProductCard/ProductCard";
import { useProductView } from '../ProductList/ProductViewContext';

const Favorites = ({ favoriteItems, updateFavoriteCount }) => {
  const handleRemoveFromFavorites = (productId) => {
  
      const updatedFavorites = favoriteItems.filter((item) => item.id !== productId);
      localStorage.setItem('favoriteItems', JSON.stringify(updatedFavorites));
      updateFavoriteCount(updatedFavorites.length);
    
  };

  const { isTableView } = useProductView();

  return (
    <div>
      <h2>Favorite Products</h2>
      {favoriteItems.length === 0 ? (
        <p>Your favorites list is empty.</p>
      ) : (
        <div className={`product-list ${isTableView ? 'table-view' : 'card-view'}`}>
          {favoriteItems.map((product) => (
            <ProductCard
              key={product.id}
              product={product}
              updateFavoriteCount={updateFavoriteCount}
              handleRemoveFromFavorites={handleRemoveFromFavorites}
              showButton={false}
            />
          ))}
        </div>
      )}
    </div>
  );
};

export { Favorites };
